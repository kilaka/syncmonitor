#!/usr/bin/env bash

function assertReturnStatus() { #params(returnStatus, message)
	if [ $1 -ne 0 ]
	then
		echo ERROR! Status: $1. Message: $2.
		read -n1 -r -p "Press any key to continue..."
		exit 1
	fi
}

mkdir -p classes
assertReturnStatus $? "Failed creating folder - classes"

#javac -source 7 -target 7 -Werror -d classes -cp "libs/*.jar" src/*
javac -source 7 -target 7 -Werror -d classes -cp "libs/*" $(find src -name *.java)
assertReturnStatus $? "Failed compiling classes"

unzip -oq "libs/*.jar" -d dep
assertReturnStatus $? "Failed extracting dependencies"

rm -rf dep/META-INF
assertReturnStatus $? "Failed deleting dependencies META-INF"

#rm -f SyncMonitor.jar
#assertReturnStatus $? "Failed deleting old jar"
#jar cfm SyncMonitor.jar META-INF/MANIFEST.MF -C classes . -C libs .
jar cfe SyncMonitor.jar info.fastpace.syncmonitor.SyncMonitor -C classes . -C dep .
assertReturnStatus $? "Failed creating jar"

rm -rf classes
assertReturnStatus $? "Failed cleaning up classes folder"
rm -rf dep
assertReturnStatus $? "Failed cleaning up dep - extracted dependencies"

cat stub.sh SyncMonitor.jar > SyncMonitor.sh
assertReturnStatus $? "Failed creating executable"

chmod +x SyncMonitor.sh
assertReturnStatus $? "Failed changing executable permissions"