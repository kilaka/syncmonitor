package info.fastpace.syncmonitor;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SyncMonitor {

	@Parameter(names = "-src", description = "Source folder to monitor", required = true)
	private String sourceFolder;

	@Parameter(names = "-dst", description = "Destination folder to for the changed files", required = true)
	private String destFolder;

	@Parameter(names = "--help", help = true, description = "Shows this help")
	private boolean help;

	public static void main(String[] args) throws Exception {
		SyncMonitor syncMonitor = new SyncMonitor();
		JCommander jCommander = new JCommander(syncMonitor);
		jCommander.setProgramName(new Object() {
		}.getClass().getEnclosingClass().getName());
		try {
			jCommander.parse(args);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			usage(jCommander);
			System.exit(1);
		}

		if (syncMonitor.help) {
			usage(jCommander);
			return;
		}

		syncMonitor.sync();
	}

	private static void usage(JCommander jCommander) {
		jCommander.usage();
		System.out.println("Monitors the source folder and on changes copies changed files to the destination folder");
		System.out.println("* upon execution, copies immediately the source folder to the destination folder");
	}

	private void sync() throws IOException {
		final Path sourcePath = new File(sourceFolder).toPath();
		final Path destinationFolder = new File(destFolder).toPath();
		final ScheduledThreadPoolExecutor scheduledExecutorService = new ScheduledThreadPoolExecutor(1);
//		final Timer timer = new Timer();
		final ConcurrentHashMap<CopyTask, Object> tasks = new ConcurrentHashMap<>();
		WatchDir watchDir = new WatchDir(sourcePath, true) {
			@Override
			protected void handle(Path child) {
				File src = child.toFile();
				if (!src.exists() || src.isDirectory()) return;
				Path relative = sourcePath.relativize(child);
				File dst = destinationFolder.resolve(relative).toFile();
				CopyTask copyTask = new CopyTask(src, dst);
				tasks.put(copyTask, this);
				scheduledExecutorService.getQueue().clear();

				Runnable runnable = new Runnable() {
					@Override
					public void run() {
						while (!tasks.isEmpty()) {
							Iterator<CopyTask> iterator = tasks.keySet().iterator();
							CopyTask copyTask = iterator.next();
							iterator.remove();
							copyTask.run();
						}
					}
				};
				scheduledExecutorService.schedule(runnable, 2, TimeUnit.SECONDS);
			}
		};
		watchDir.processEvents();
	}

	private static class CopyTask implements Runnable {
		private File src;
		private File dst;

		public CopyTask(File src, File dst) {
			this.src = src;
			this.dst = dst;
		}

		@Override
		public void run() {
			if (!src.exists()) {
				if (dst.exists()) {
					FileUtils.deleteQuietly(dst);
					System.out.println(new Date() + " - Deleted from '" + src + "' to '" + dst + "'");
				}
				return;
			}
			try {
				FileUtils.copyFile(src, dst);
			} catch (IOException e) {
				// TODO: Add issue to the OS notifications center
				System.out.println("Fail - "+new Date() + " - Copied from '" + src + "' to '" + dst + "'");
			}
			System.out.println(new Date() + " - Copied from '" + src + "' to '" + dst + "'");
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			CopyTask copyTask = (CopyTask) o;

			if (!src.equals(copyTask.src)) return false;
			return dst.equals(copyTask.dst);
		}

		@Override
		public int hashCode() {
			int result = src.hashCode();
			result = 31 * result + dst.hashCode();
			return result;
		}
	}
//	public static void main(String[] args) throws Exception {
//		Options options = new Options();
//		options.addOption("help", true, "print this message");
////		OptionGroup optionGroup = new OptionGroup();
////		optionGroup.addOption(new Option("source", null));
////		options.addOptionGroup(optionGroup);
//		CommandLineParser parser = new BasicParser();
//		CommandLine cmd = parser.parse(options, args);
//
//		if (cmd.hasOption("help")) {
//			HelpFormatter formatter = new HelpFormatter();
//			formatter.printHelp(HelpFormatter.DEFAULT_WIDTH, "syncMonitor [OPTIONS] source-folder dest-folder", "Monitors the source folder and on changes copies changed files to the destination folder", options, "* upon execution, copies immediately the source folder to the destination folder", false);
//			return;
//		}
//
//		List<String> argList = cmd.getArgList();
//		if (argList.size() != 2) {
//			throw new Exception("Arguments list should contain source and destination as free args. Found: " + Arrays.toString(argList.toArray()));
//		}
//	}
}
